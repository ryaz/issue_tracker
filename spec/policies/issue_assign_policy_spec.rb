require 'rails_helper'

RSpec.describe IssueAssignPolicy do
  let(:user) { create(:user) }
  let(:manager) { create(:user, role: 'manager') }
  let(:issue) { create(:issue) }

  subject { described_class }
  permissions :update? do
    context 'manager' do
      it 'allowed to assign' do
        expect(subject).to permit(manager, issue)
      end
    end

    context 'user' do
      it 'not allowed to assign' do
        expect(subject).not_to permit(user, issue)
      end
    end
  end
end
