require 'rails_helper'

RSpec.describe IssueStatusPolicy do
  let(:user) { create(:user) }
  let(:manager) { create(:user, role: 'manager') }
  let(:issue) { create(:issue, assignee: user) }

  subject { described_class }

  permissions :update? do
    it 'not allowed if not assigned to manager' do
      expect(subject).not_to permit(manager, issue)
    end

    it 'allowed if assigned to user' do
      expect(subject).to permit(user, issue)
    end
  end
end
