require 'rails_helper'

RSpec.describe IssuePolicy do
  let(:user) { create(:user) }
  let(:manager) { create(:user, role: 'manager') }
  let(:issue) { create(:issue, user: user) }

  subject { described_class }

  permissions :update? do
    context 'manager' do
      it 'not allowed user issue' do
        expect(subject).not_to permit(manager, issue)
      end
    end

    context 'user' do
      it 'allowed own issue' do
        expect(subject).to permit(user, issue)
      end
    end
  end
end
