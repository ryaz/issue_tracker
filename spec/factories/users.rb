# frozen_string_literal: true

FactoryGirl.define do
  factory :user do
    sequence(:email) { |num| "someone#{num}@example.com" }
    password 'supersecret'
  end
end
