require 'rails_helper'

RSpec.describe Issue, type: :model do
  describe '#toggle_assign_to' do
    let(:user) { create(:user) }
    let(:issue) { create(:issue) }
    let(:assigned_issue) { create(:issue, assignee: user) }

    subject { issue.assignee_id }

    it 'assigns user if no assigned' do
      issue.toggle_assign_to(user)
      expect(subject).to be user.id
    end

    it 'unassigns if assigned to user' do
      assigned_issue.toggle_assign_to(user)
      expect(subject).to be nil
    end

    it 'do nothing if assigned to another user' do
      another_user = create(:user)
      assigned_to_another_user_issue = create(:issue, assignee: another_user)
      assigned_to_another_user_issue.toggle_assign_to(user)
      expect(assigned_to_another_user_issue.assignee_id).to be another_user.id
    end
  end
end
