require 'rails_helper'

RSpec.describe Api::V1::IssueStatusesController, type: :controller do
  render_views

  let(:user) { create(:user, role: 'manager') }
  let(:params) { { issue: { status: 'in_progress' } } }

  context '#update' do
    context 'manager can change status for self assigned' do
      let(:issue) { create(:issue, assignee: user) }
      subject do
        request.headers.merge(auth_header(user))
        put :update, format: :json, params: params.merge(id: issue.id)
      end

      it 'responds successfully ' do
        subject
        expect(response).to have_http_status :ok
      end
    end

    context 'manager can not change status for not assigned' do
      let(:issue) { create(:issue) }
      subject do
        request.headers.merge(auth_header(user))
        put :update, format: :json, params: params.merge(id: issue.id)
      end

      it 'responds with unauthorized' do
        subject
        expect(response).to have_http_status :unauthorized
      end
    end
  end

  def auth_header(usr)
    token = Knock::AuthToken.new(payload: { sub: usr.id }).token
    { 'Authorization' => "Bearer #{token}" }
  end
end
