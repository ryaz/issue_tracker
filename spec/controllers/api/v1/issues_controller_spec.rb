require 'rails_helper'

RSpec.describe Api::V1::IssuesController, type: :controller do
  render_views

  let(:user) { create(:user) }
  let(:params) { { issue: { title: 'my awesome issue' } } }

  context '#create' do
    context 'user' do
      let(:subject) do
        request.headers.merge(auth_header(user))
        post :create, format: :json, params: params
      end

      it { expect { subject }.to change { Issue.count }.by(1) }

      it 'responds successfully' do
        subject
        expect(response).to have_http_status :created
      end
    end
  end

  context '#update' do
    context 'user' do
      let(:user_issue) { create(:issue, user: user) }
      let(:subject) do
        request.headers.merge(auth_header(user))
        put :update, format: :json, params: params.merge(id: user_issue.id)
      end

      it 'responds successfully' do
        subject
        expect(response).to have_http_status :ok
      end
    end
  end

  context '#destroy' do
    context 'user' do
      let!(:user_issue) { create(:issue, user: user) }
      let(:subject) do
        request.headers.merge(auth_header(user))
        delete :destroy, format: :json, params: { id: user_issue.id }
      end

      it 'responds successfully' do
        subject
        expect(response).to have_http_status :ok
      end

      it { expect { subject }.to change { Issue.count }.by(-1) }
    end
  end

  context '#index' do
    let!(:issue) { create(:issue) }
    let(:subject) do
      request.headers.merge(auth_header(user))
      get :index, format: :json
    end

    context 'user' do
      let!(:user_issue) { create(:issue, user: user) }

      it 'responds successfully' do
        subject
        expect(response).to have_http_status :success
      end

      it 'can see own issue and only' do
        subject
        expect_json_sizes(1)
        expect_json('?', id: user_issue.id)
      end
    end

    context 'manager' do
      let(:user) { create(:user, role: :manager) }
      let!(:manager_issue) { create(:issue, user: user) }

      it 'responds successfully' do
        subject
        expect(response).to have_http_status :success
      end

      it 'can see other user issue' do
        subject
        expect_json('?', id: issue.id)
      end

      it 'can see own issue' do
        subject
        expect_json('?', id: manager_issue.id)
      end

      it 'can see all issues' do
        subject
        expect_json_sizes(2)
      end
    end
  end

  context '#show' do
    let(:subject) do
      request.headers.merge(auth_header(user))
      get :show, format: :json, params: { id: id }
    end
    let!(:other_user_issue) { create(:issue) }
    let!(:user_issue) { create(:issue, user: user) }
    let(:id) { user_issue.id }

    context 'user' do
      it 'can see own issue' do
        subject
        expect(response).to have_http_status :success
        expect_json(id: id)
      end

      it 'cant see other user issue' do
        request.headers.merge(auth_header(user))
        get :show, format: :json, params: { id: other_user_issue.id }
        expect(response).to have_http_status :unauthorized
      end
    end

    context 'manager' do
      let(:user) { create(:user, role: :manager) }
      let(:id) { user_issue.id }

      it 'can see user issue' do
        subject
        expect(response).to have_http_status :success
        expect_json(id: id)
      end

      it 'can see other user issue' do
        request.headers.merge(auth_header(user))
        get :show, format: :json, params: { id: other_user_issue.id }
        expect(response).to have_http_status :success
        expect_json(id: other_user_issue.id)
      end
    end
  end

  def auth_header(usr)
    token = Knock::AuthToken.new(payload: { sub: usr.id }).token
    { 'Authorization' => "Bearer #{token}" }
  end
end
