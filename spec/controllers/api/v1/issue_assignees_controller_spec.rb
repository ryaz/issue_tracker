require 'rails_helper'

RSpec.describe Api::V1::IssueAssigneesController, type: :controller do
  render_views

  let(:user) { create(:user) }
  let(:manager) { create(:user, role: 'manager') }

  context '#update' do
    context 'manager' do
      let(:issue) { create(:issue, user: manager) }
      subject do
        request.headers.merge(auth_header(manager))
        put :update, format: :json, params: { id: issue.id }
      end

      it 'assigns to himself unassigned' do
        subject
        expect(response).to have_http_status :ok
      end

      it 'unassigns unassigned to himself' do
        assigned_issue = create(:issue, assignee: manager)
        request.headers.merge(auth_header(manager))
        put :update, format: :json, params: { id: assigned_issue.id }
        expect(response).to have_http_status :ok
      end
    end

    context 'user' do
      let(:issue) { create(:issue, user: user) }
      subject do
        request.headers.merge(auth_header(user))
        put :update, format: :json, params: { id: issue.id }
      end

      it 'cant assign to himself unassigned' do
        subject
        expect(response).not_to have_http_status :ok
      end
    end
  end

  def auth_header(usr)
    token = Knock::AuthToken.new(payload: { sub: usr.id }).token
    { 'Authorization' => "Bearer #{token}" }
  end
end
