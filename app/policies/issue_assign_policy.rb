class IssueAssignPolicy < ApplicationPolicy
  def update?
    user.manager?
  end
end
