class IssueStatusPolicy < ApplicationPolicy
  def update?
    record.assignee_id == user.id
  end
end
