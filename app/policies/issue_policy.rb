class IssuePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      user.manager? ? scope : scope.where(user: user)
    end
  end

  def update?
    record.user_id == user.id
  end

  def destroy?
    update?
  end

  def show?
    if user.manager?
      super
    else
      scope.where(id: record.id, user_id: user.id).exists?
    end
  end
end
