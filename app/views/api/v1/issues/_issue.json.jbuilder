json.extract! issue, :id, :title, :user_id, :assignee_id, :created_at, :updated_at
json.url api_v1_issue_url(issue, format: :json)
