class User < ApplicationRecord
  has_secure_password

  ROLES = %w(manager).freeze

  has_many :issues
  has_many :assigned_issues, foreign_key: :assignee_id

  validates :role, inclusion: { in: ROLES }, allow_nil: true
  validates :email, :password, presence: true

  def manager?
    role == 'manager'
  end
end
