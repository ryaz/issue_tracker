class Issue < ApplicationRecord
  belongs_to :user
  belongs_to :assignee, class_name: 'User', foreign_key: :assignee_id, optional: true

  enum status: { pending: 0, in_progress: 1, resolved: 2 }

  scope :by_status, ->(stat) { where(status: stat) if statuses.keys.include?(stat) }

  validates :status, inclusion: { in: statuses.keys }
  validates :assignee, presence: true, if: :assignee_needed?

  def assignee_needed?
    Issue.statuses.keys.last(2).include?(status)
  end

  def toggle_assign_to(user)
    if assignee_id.blank?
      update(assignee_id: user.id)
    elsif assignee_id == user.id
      update(assignee_id: nil)
    end
  end
end
