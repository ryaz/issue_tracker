class Api::V1::IssueStatusesController < Api::BaseController
  def update
    @issue = Issue.find(params[:id])
    unless IssueStatusPolicy.new(current_user, @issue).update?
      raise Pundit::NotAuthorizedError, 'not allowed to change status'
    end
    if @issue.update(issue_params)
      render status: :ok, json: {}
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  private

  def issue_params
    params.require(:issue).permit(:status)
  end
end
