class Api::V1::IssueAssigneesController < Api::BaseController
  def update
    @issue = Issue.find(params[:id])
    unless IssueAssignPolicy.new(current_user, @issue).update?
      raise Pundit::NotAuthorizedError, 'not allowed to change assignee'
    end
    if @issue.toggle_assign_to(current_user)
      render status: :ok, json: {}
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end
end
