class Api::V1::IssuesController < Api::BaseController
  before_action :set_issue, only: [:show, :update, :destroy]

  def index
    issues = Issue.by_status(params[:status]).order(created_at: :desc)
    @issues = policy_scope(issues).page(params[:page])
  end

  def show
    authorize @issue
  end

  def create
    @issue = current_user.issues.new(issue_params)

    if @issue.save
      render status: :created, json: {}
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  def update
    authorize @issue
    if @issue.update(issue_params)
      render status: :ok, json: {}
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  def destroy
    authorize @issue
    if @issue.destroy
      render status: :ok, json: {}
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  private

  def set_issue
    @issue = Issue.find(params[:id])
  end

  def issue_params
    params.require(:issue).permit(:title)
  end
end
