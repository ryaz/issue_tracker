class CreateIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :issues do |t|
      t.string :title
      t.belongs_to :user, foreign_key: true
      t.belongs_to :assignee, foreign_key: true
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
