Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :issues, defaults: { format: :json }
      resources :users, defaults: { format: :json }
      resources :issue_assignees, only: :update, defaults: { format: :json }
      resources :issue_statuses, only: :update, defaults: { format: :json }
    end
  end
  post 'user_token' => 'user_token#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
